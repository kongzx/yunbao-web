import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Goods from '@/components/basedata/Goods'
import Storage from '@/components/basedata/Storage'
import GoodsUnit from '@/components/basedata/GoodsUnit'
import GoodsCategory from '@/components/basedata/GoodsCategory'
import Account from '@/components/basedata/Account'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: '首页',
      component: Home,
      children: [
        {
          name: '商品列表',
          path: '/basedata/goods',
          component: Goods
        },
        {
          name: '商品单位',
          path: '/basedata/goodsunit',
          component: GoodsUnit
        },
        {
          name: '商品分类',
          path: '/basedata/goodscategory',
          component: GoodsCategory
        },
        {
          name: '仓库列表',
          path: '/basedata/storage',
          component: Storage
        },
        {
          name: '结算账户',
          path: '/basedata/account',
          component: Account
        }
      ]
    }
  ]
})
